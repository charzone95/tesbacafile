package id.web.charzone95.cobabacafile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public static final int CHOOSE_FILE_REQUEST_CODE = 1001;

    @BindView(R.id.button_pick)
    AppCompatButton buttonPick;

    @BindView(R.id.textview_path)
    TextView textViewPath;

    @BindView(R.id.textview_content)
    TextView textViewContent;

    String filePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.button_pick)
    public void buttonPickClick() {

        //check permission
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            chooseFile();
                        } else {
                            Toast.makeText(MainActivity.this, "Please allow all permissions!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    private void chooseFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(Intent.createChooser(intent, "Pilih file"), CHOOSE_FILE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();

            if (uri != null) {
                processChosenFile(uri);
            }
        }
    }

    private void processChosenFile(@NonNull Uri uri) {
        //ambil nama file, mana tau perlu
        filePath = FileHelper.getPath(this, uri);
        textViewPath.setText(filePath);


        //baca isi file
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);

            //kita anggap saja gak null
            assert inputStream != null;
            String line = "";
            StringBuilder hasil = new StringBuilder();

            // source: https://stackoverflow.com/a/5333971/3433809
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }
            byte[] bytes = bos.toByteArray();

            System.out.println("Bytes length: " + bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                System.out.println("Bytes[" + i + "] --> " + bytes[i]);
            }

        } catch (IOException e) {
            Toast.makeText(this, "Gagal baca file", Toast.LENGTH_SHORT).show();
        }
    }
}
